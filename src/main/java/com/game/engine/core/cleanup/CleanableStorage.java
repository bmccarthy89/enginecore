package com.game.engine.core.cleanup;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class CleanableStorage {

    private final Set<Cleanable> cleanupList = new CopyOnWriteArraySet<>();
    private static final Object lock = new Object();
    private static CleanableStorage instance = null;

    private CleanableStorage() {}

    public static CleanableStorage getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new CleanableStorage();
            }
        }
        return instance;
    }

    public void subscribe(Cleanable cleanup) {
        cleanupList.add(cleanup);
    }

    public void unsubscribe(Cleanable cleanup) {
        cleanupList.remove(cleanup);
    }

    public void doClean() {
        cleanupList.forEach(Cleanable::clean);
        for (Cleanable cleanup: cleanupList) {
            cleanup.clean();
        }
    }
}
