package com.game.engine.core.cleanup;

public interface Cleanable {
    void clean();
}
