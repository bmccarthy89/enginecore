package com.game.engine.core;

import com.game.engine.core.camera.Camera;
import com.game.engine.core.entity.Entity;
import com.game.engine.core.eventing.EventManager;
import com.game.engine.core.io.Window;
import com.game.engine.core.rendering.Mesh;
import com.game.engine.core.rendering.Shader;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GLUtil;

import java.util.List;

import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL30.glUseProgram;

public class GameLoop {

    private void createObjects(Mesh triangle) {
        int[] indices = {
            0, 3, 1,
            1, 3, 2,
            2, 3, 0,
            0, 1, 2
        };
        float[] vertices = {
                 0.0f,  1.0f, 0.0f,
                -1.0f, -1.0f, 1.0f,
                 0.0f, -1.0f, -1.0f,
                 1.0f, -1.0f, 1.0f
        };
        triangle.createMesh(vertices, indices);
    }

    public void doLoop(long window) {
        EventManager manager = new EventManager();

        GLUtil.setupDebugMessageCallback();
        Mesh triangle = new Mesh();
        Entity entity1 = new Entity(triangle);
        List<Entity> entities = List.of(entity1);

        createObjects(triangle);

        Shader shader = new Shader();
        shader.createFromFile("shaders/vertex.sh", "shaders/fragment.sh");

        Matrix4f projection = new Matrix4f();
        projection.perspective(45.0f, (float)(Window.WIDTH / Window.HEIGHT), 0.1f, 100.f);
        float[] projectionMat4 = projection.get(new float[16]);

        int loopCount = 0;
        while (!glfwWindowShouldClose(window)) {
            manager.processEvents();

            // clear the framebuffer
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            shader.useShader();
            for (int i = 0; i < entities.size(); i++) {
                Entity entity = entities.get(i);
                Mesh mesh = entity.getMesh();
                mesh.renderMesh();

                float[] modelMat4 = entity.getModelAsArray();

                glUniformMatrix4fv(shader.getUniformModel(), false, modelMat4);
                glUniformMatrix4fv(shader.getUniformProjection(), false, projectionMat4);
                glUniformMatrix4fv(shader.getUniformView(), false, Camera.getInstance().calculateViewMatrix().get(new float[16]));
            }

            glUseProgram(0);

            // swap the color buffers
            glfwSwapBuffers(window);
            //Get and handle user input event
            glfwPollEvents();
            loopCount++;
            if (loopCount > 1) {
                loopCount = 0;
            }
        }
        triangle.forceClean();
        shader.forceClean();
    }
}
