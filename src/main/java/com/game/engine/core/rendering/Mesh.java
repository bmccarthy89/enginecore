package com.game.engine.core.rendering;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.cleanup.Cleanable;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

/**
 * Contains the model information, points, indices etc.
 */
public class Mesh implements Cleanable {

    private int VAO, VBO, IBO, indicesCount;

    public Mesh() {
        VAO = 0;
        VBO = 0;
        IBO = 0;
        indicesCount = 0;
        CleanableStorage.getInstance()
                .subscribe(this);
    }

    public void createMesh(float[] vertices, int[] indices) {
        indicesCount = indices.length;
        VAO = glGenVertexArrays();
        glBindVertexArray(VAO);

        IBO = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

        VBO = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

        //index refers to the shader layout
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    public void renderMesh() {
        glBindVertexArray(VAO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
        glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    /**
     * ensures the mesh is destroyed and no longer link to the GPU.
     */
    public void clean() {
        if (IBO != 0) {
           glDeleteBuffers(IBO);
           IBO = 0;
        }
        if (VBO != 0) {
            glDeleteBuffers(VBO);
            VBO = 0;
        }
        if (VAO != 0) {
            glDeleteVertexArrays(VAO);
            VAO = 0;
        }
        indicesCount = 0;
        CleanableStorage.getInstance().unsubscribe(this);
    }

    public void forceClean() {
        clean();
    }
}
