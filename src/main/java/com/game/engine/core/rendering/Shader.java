package com.game.engine.core.rendering;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.cleanup.Cleanable;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;

public class Shader implements Cleanable {
    private int shaderID;
    private int uniformProjection;
    private int uniformModel;
    private int uniformView;

    public Shader() {
        shaderID = 0;
        uniformProjection = 0;
        uniformModel = 0;
        uniformView = 0;
        CleanableStorage.getInstance().subscribe(this);
    }

    public void createFromFile(String vertexFilepath, String fragmentFilepath) {
        compileShaders(vertexFilepath, fragmentFilepath);
    }

    public void useShader() {
        glUseProgram(shaderID);
    }

    public void clean() {
        if (shaderID != 0) {
            glDeleteProgram(shaderID);
            shaderID = 0;
        }
        uniformProjection = 0;
        uniformModel = 0;
        uniformView = 0;
    }

    public void forceClean() {
        clean();
        CleanableStorage.getInstance().unsubscribe(this);
    }

    private void compileShaders(String vertexFilepath, String fragmentFilepath) {
        shaderID = glCreateProgram();
        if(shaderID == 0) {
            System.out.println("Error creating shader program!");
        }

        addShader(shaderID, vertexFilepath, GL_VERTEX_SHADER);
        addShader(shaderID, fragmentFilepath, GL_FRAGMENT_SHADER);

        glLinkProgram(shaderID);

        if (glGetProgrami(shaderID, GL_LINK_STATUS) == GL_FALSE) {
            System.out.println("Error linking shader.");
        }

        glValidateProgram(shaderID);

        if (glGetProgrami(shaderID, GL_VALIDATE_STATUS) == GL_FALSE) {
            System.out.println("Error validating shader.");
        }

        uniformProjection = glGetUniformLocation(shaderID, "projection");
        uniformModel = glGetUniformLocation(shaderID, "model");
        uniformView = glGetUniformLocation(shaderID, "view");
    }

    private void addShader(int shaderId, String fileName, int shaderType) {
        int shader = glCreateShader(shaderType);
        String shaderCode = loadShader(fileName);

        glShaderSource(shader, shaderCode);
        glCompileShader(shader);

        if(glGetShaderi(shader,GL_COMPILE_STATUS) == GL_FALSE){
            System.out.println("Error compiling shader. " + fileName);
        }

        glAttachShader(shaderId, shader);
    }

    private String loadShader(String fileName) {
        ShaderReader reader = ShaderReader.getInstance();
        return reader.read(fileName);
    }

    public int getUniformModel() {
        return uniformModel;
    }

    public int getUniformProjection() {
        return uniformProjection;
    }

    public int getUniformView() {
        return uniformView;
    }
}
