package com.game.engine.core.rendering;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.cleanup.Cleanable;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ShaderReader implements Cleanable {

    private static ShaderReader instance;
    private static final Object lock = new Object();

    private final HashMap<String, String> shadersByFileLocation = new HashMap<>();

    private ShaderReader() {
        CleanableStorage.getInstance().subscribe(this);
    }

    public static ShaderReader getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new ShaderReader();
            }
        }
        return instance;
    }

    public String read(String fileLocation) {
        String shader = "";
        String shaderFile = shadersByFileLocation.get(fileLocation);
        if (shaderFile != null) {
            return shaderFile;
        }
        String fileAsResource = getClass().getClassLoader().getResource(fileLocation).getFile();
        try (FileInputStream fileInputStream = new FileInputStream(fileAsResource);
             BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {
                shader = shader + "\n" + line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        shadersByFileLocation.put(fileLocation, shader);
        return shader;
    }

    public void clean() {
        shadersByFileLocation.clear();
    }
}
