package com.game.engine.core.io.eventing.serializers;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventSerializer;
import com.game.engine.core.io.eventing.events.ButtonReleasedEvent;

public class ButtonReleasedEventSerializer implements EventSerializer {

    @Override
    public void write(Event event) {

    }

    @Override
    public Event read() {
        return null;
    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof ButtonReleasedEvent;
    }
}
