package com.game.engine.core.io;

public class KeyStateStore {

    private final boolean[] keys = new boolean[1024];
    private static final Object lock = new Object();
    private static KeyStateStore instance = null;

    private KeyStateStore() {}

    public static KeyStateStore getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new KeyStateStore();
            }
        }
        return instance;
    }

    public void keyPressed(int key) {
        keys[key] = true;
    }

    public void keyReleased(int key) {
        keys[key] = false;
    }

    public boolean keyIsDown(int key) {
        return keys[key];
    }
}
