package com.game.engine.core.io.eventing.events;

import com.game.engine.core.eventing.api.Event;

public class KeyPressedEvent implements Event {
    private final long windowId;
    private final int key;
    private final int action;

    public KeyPressedEvent(long windowId, int key, int action){
        this.windowId = windowId;
        this.key = key;
        this.action = action;
    }

    public long getWindowId() {
        return windowId;
    }

    public int getKey() {
        return key;
    }

    public int getAction() {
        return action;
    }
}
