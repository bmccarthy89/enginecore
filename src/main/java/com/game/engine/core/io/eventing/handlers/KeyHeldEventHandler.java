package com.game.engine.core.io.eventing.handlers;

import com.game.engine.core.camera.Camera;
import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.io.eventing.events.KeyHeldEvent;
import com.game.engine.core.io.eventing.events.KeyPressedEvent;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

public class KeyHeldEventHandler implements EventHandler {

    @Override
    public void handle(Event event) {
        KeyHeldEvent keyHeldEvent = (KeyHeldEvent) event;

        int key = keyHeldEvent.getKey();

        Camera camera = Camera.getInstance();
        if (key == GLFW_KEY_W) {
            camera.moveForward();
        }

        if (key == GLFW_KEY_A) {
            camera.moveLeft();
        }

        if (key == GLFW_KEY_S) {
            camera.moveBack();
        }

        if (key == GLFW_KEY_D) {
            camera.moveRight();
        }
    }

    @Override
    public void clean() {

    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof KeyHeldEvent;
    }
}
