package com.game.engine.core.io.eventing.events;

import com.game.engine.core.eventing.api.Event;

public class MousePositionUpdatedEvent implements Event {

    private final double yChange;
    private final double xChange;

    public MousePositionUpdatedEvent(double xChange, double yChange) {
        this.xChange = xChange;
        this.yChange = yChange;
    }

    public double getXChange() {
        return xChange;
    }

    public double getYChange() {
        return yChange;
    }
}
