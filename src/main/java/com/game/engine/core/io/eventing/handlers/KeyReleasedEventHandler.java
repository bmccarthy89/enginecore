package com.game.engine.core.io.eventing.handlers;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.io.KeyStateStore;
import com.game.engine.core.io.eventing.events.KeyReleasedEvent;

public class KeyReleasedEventHandler implements EventHandler {
    @Override
    public void clean() {

    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof KeyReleasedEvent;
    }

    @Override
    public void handle(Event event) {
        KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent) event;

        KeyStateStore keyStateStore = KeyStateStore.getInstance();
        keyStateStore.keyReleased(keyReleasedEvent.getKey());
    }
}
