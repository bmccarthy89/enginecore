package com.game.engine.core.io;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.eventing.EventQueueImpl;
import com.game.engine.core.eventing.api.EventQueue;
import com.game.engine.core.eventing.api.Feed;
import com.game.engine.core.io.eventing.events.KeyHeldEvent;

public class KeyHeldEventFeed implements Feed {
    private final KeyStateStore keyStateStore = KeyStateStore.getInstance();
    private final EventQueue eventQueue = EventQueueImpl.getInstance();
    private boolean signal = true;

    public KeyHeldEventFeed() {
        CleanableStorage.getInstance().subscribe(this);
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        while(signal) {
            long current = System.currentTimeMillis();
            if (current - start > 200) {
                for (int i = 0; i < 1024; i++) {
                    if (keyStateStore.keyIsDown(i)) {
                        eventQueue.add(new KeyHeldEvent(i));
                    }
                }
                start = current;
            }
        }
    }

    @Override
    public void clean() {
        signal = false;
        CleanableStorage.getInstance().unsubscribe(this);
    }
}
