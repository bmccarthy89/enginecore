package com.game.engine.core.io.eventing.events;

import com.game.engine.core.eventing.api.Event;

public class ButtonClickedEvent implements Event {

    private final int button;

    public ButtonClickedEvent(int button) {
        this.button = button;
    }

    public int getButton() {
        return button;
    }
}
