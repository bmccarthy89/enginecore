package com.game.engine.core.io.eventing.handlers;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.io.KeyStateStore;
import com.game.engine.core.io.eventing.events.KeyPressedEvent;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;

public class KeyPressedEventHandler implements EventHandler {

    @Override
    public void handle(Event event) {
        KeyPressedEvent keyPressedEvent = (KeyPressedEvent) event;
        int key = keyPressedEvent.getKey();
        int action = keyPressedEvent.getAction();
        long windowId = keyPressedEvent.getWindowId();

        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            glfwSetWindowShouldClose(windowId, true);
        }

        KeyStateStore keyStateStore = KeyStateStore.getInstance();
        keyStateStore.keyPressed(key);
    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof KeyPressedEvent;
    }

    @Override
    public void clean() {

    }
}
