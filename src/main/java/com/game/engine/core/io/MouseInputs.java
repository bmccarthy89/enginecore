package com.game.engine.core.io;

import com.game.engine.core.eventing.EventQueueImpl;
import com.game.engine.core.eventing.api.EventQueue;
import com.game.engine.core.io.eventing.events.ButtonClickedEvent;
import com.game.engine.core.io.eventing.events.ButtonReleasedEvent;
import com.game.engine.core.io.eventing.events.MousePositionUpdatedEvent;

import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_DISABLED;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

public class MouseInputs {

    private final long windowId;
    private double lastX;
    private double lastY;
    private boolean mouseFirstMoved = true;
    private final EventQueue queue = EventQueueImpl.getInstance();

    public MouseInputs(long windowId) {
        this.windowId = windowId;
        glfwSetInputMode(windowId, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    public void createCallbacks() {
        glfwSetMouseButtonCallback(windowId, this::mouseButtonPressed);
        glfwSetCursorPosCallback(windowId, this::mouseMoved);
    }

    public void mouseMoved(long windowId, double xPos, double yPos) {
        if  (mouseFirstMoved) {
            lastX = xPos;
            lastY = yPos;
            mouseFirstMoved = false;
        }
        double xChange = xPos - lastX;
        //This prevents inverted controls
        double yChange = lastY - yPos;

        lastX = xPos;
        lastY = yPos;

        queue.add(new MousePositionUpdatedEvent(xChange, yChange));
    }

    public void mouseButtonPressed(long windowId, int button, int action, int mods) {
        if (action == GLFW_PRESS) {
            queue.add(new ButtonClickedEvent(button));
        } else if (action == GLFW_RELEASE) {
            queue.add(new ButtonReleasedEvent(button));
        }
    }
}
