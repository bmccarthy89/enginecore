package com.game.engine.core.io.eventing.handlers;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.io.eventing.events.MousePositionUpdatedEvent;

public class MousePositionUpdatedEventHandler implements EventHandler {
    @Override
    public void clean() {

    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof MousePositionUpdatedEvent;
    }

    @Override
    public void handle(Event event) {
        MousePositionUpdatedEvent mousePosition = (MousePositionUpdatedEvent) event;
    }
}
