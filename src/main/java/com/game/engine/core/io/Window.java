package com.game.engine.core.io;

import com.game.engine.core.cleanup.Cleanable;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window implements Cleanable {

    public static final int WIDTH = 1080;
    public static final int HEIGHT = 790;
    private long windowId;

    public void createMainWindow() {
        //Set up error printing
        GLFWErrorCallback.createPrint(System.err).set();

        //Initialise GLFW
        if (!glfwInit()) {
            System.out.println("GLFW initialisation failed.");
            glfwTerminate();
            System.exit(1);
        }

        //Setup GLFW window properties
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        //Core profile, No backwards compatibility
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        //Allow forward compatibility
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

        windowId = glfwCreateWindow(WIDTH, HEIGHT, "Game Window", NULL, NULL);
        if (windowId == NULL) {
            System.out.println("GLFW Window creation failed");
            glfwTerminate();
            System.exit(1);
        }
        //Set context for GLEW to use
        glfwMakeContextCurrent(windowId);

        glfwSwapInterval(1);

        glfwShowWindow(windowId);

        GL.createCapabilities();

        glClearColor(0.0157f, 0.3882f, 0.0275f, 1.0f);


        glEnable(GL_DEPTH_TEST);
    }

    public void clean() {
        if (windowId != 0) {
            glfwDestroyWindow(windowId);
            windowId = 0;
        }
    }

    public long getWindowId() {
        return windowId;
    }
}
