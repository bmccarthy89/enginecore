package com.game.engine.core.io;

import com.game.engine.core.eventing.EventQueueImpl;
import com.game.engine.core.eventing.api.EventQueue;
import com.game.engine.core.io.eventing.events.KeyPressedEvent;
import com.game.engine.core.io.eventing.events.KeyReleasedEvent;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;

public class KeyboardInputs {

    private final long windowId;
    private final EventQueue queue = EventQueueImpl.getInstance();

    public KeyboardInputs(long windowId) {
        this.windowId = windowId;
    }

    public void createCallbacks() {
        glfwSetKeyCallback(windowId, this::handleKeys);
    }

    public void handleKeys(long windowId, int key, int code, int action, int mode) {
        if (action == GLFW_PRESS) {
            queue.add(new KeyPressedEvent(windowId, key, action));
        } else if (action == GLFW_RELEASE) {
            queue.add(new KeyReleasedEvent(key));
        }
    }
}
