package com.game.engine.core.io.eventing.events;

import com.game.engine.core.eventing.api.Event;

public class KeyHeldEvent implements Event {

    private final int key;

    public KeyHeldEvent(int key){
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
