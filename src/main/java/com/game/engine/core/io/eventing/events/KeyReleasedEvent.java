package com.game.engine.core.io.eventing.events;

import com.game.engine.core.eventing.api.Event;

public class KeyReleasedEvent implements Event {

    private final int key;

    public KeyReleasedEvent(int key){
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
