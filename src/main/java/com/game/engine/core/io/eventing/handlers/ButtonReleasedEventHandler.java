package com.game.engine.core.io.eventing.handlers;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.io.eventing.events.ButtonReleasedEvent;

public class ButtonReleasedEventHandler implements EventHandler {

    @Override
    public void clean() {

    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof ButtonReleasedEvent;
    }

    @Override
    public void handle(Event event) {
        ButtonReleasedEvent buttonReleasedEvent = (ButtonReleasedEvent) event;
    }
}
