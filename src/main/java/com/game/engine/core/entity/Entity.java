package com.game.engine.core.entity;

import com.game.engine.core.rendering.Mesh;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Entity {
    private final Mesh mesh;
    private final Matrix4f model;

    public Entity(Mesh mesh) {
        this.mesh = mesh;
        this.model = new Matrix4f();
    }

    public void translate(float x, float y, float z) {
        model.translate(new Vector3f(x, y, z));
    }

    public void scale(float x, float y, float z) {
        model.scale(new Vector3f(x, y, z));
    }

    public void rotate(float angle, float x, float y, float z) {
        model.rotate(angle, new Vector3f(x, y, z));
    }

    public float[] getModelAsArray() {
        return model.get(new float[16]);
    }

    public Mesh getMesh() {
        return mesh;
    }
}
