package com.game.engine.core.camera;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.eventing.EventHandlerStorageImpl;
import org.joml.Math;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.joml.Math.cos;
import static org.joml.Math.sin;
import static org.joml.Math.toRadians;

public class Camera {

    private Vector3f position;
    private Vector3f front;
    private Vector3f up;
    private Vector3f right;
    private Vector3f worldUp;

    private float yaw;
    private float pitch;
    private float movementSpeed;
    private float turnSpeed;

    private static final Object lock = new Object();
    private static Camera instance = null;

    /**
     * Default initialisation
     * @return
     */
    public static Camera getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new Camera(
                        new Vector3f(0.0f, 0.0f, 0.0f),
                        new Vector3f(0.0f, 1.0f, 0.0f),
                        0.0f, 0.0f,2.0f, 0.5f);
            }
        }
        return instance;
    }

    public static Camera getInstance(Vector3f position, Vector3f up, float yaw, float pitch, float movementSpeed, float turnSpeed) {
        synchronized (lock) {
            if (instance == null) {
                instance = new Camera(position, up, yaw, pitch, movementSpeed, turnSpeed);
            }
        }
        return instance;
    }

    private Camera(Vector3f position, Vector3f up, float yaw, float pitch, float movementSpeed, float turnSpeed) {
        this.position = position;
        this.up = up;
        this.yaw = yaw;
        this.pitch = pitch;
        this.movementSpeed = movementSpeed;
        this.turnSpeed = turnSpeed;
        this.front = new Vector3f(0.0f, 0.0f, -1.0f);
        this.worldUp = new Vector3f(0.0f, 1.0f, 0.0f);
        update();
    }

    private void update() {
        float x = cos(toRadians(yaw)) * cos(toRadians(pitch));
        float y = sin(toRadians(pitch));
        float z = sin(toRadians(yaw)) * cos(toRadians(pitch));
        front = new Vector3f(x, y, z);
        front.normalize();

        right = cross(front, worldUp).normalize();
        up = cross(right, front).normalize();
    }

    public Vector3f cross(Vector3f a, Vector3f b) {
        float x = (a.y * b.z) - (a.z * b.y);
        float y = (a.z * b.x) - (a.x * b.z);
        float z = (a.x * b.y) - (a.y * b.x);
        return new Vector3f(x, y, z);
    }

    public Matrix4f calculateViewMatrix() {
        update();
        Matrix4f matrix4f = new Matrix4f()
                .lookAt(
                        position,
                        new Vector3f(position.x, position.y, position.z).add(front.x, front.y, front.z),
                        up);
        return matrix4f;
    }

    public void moveForward() {
        position.add(new Vector3f(front.x, front.y, front.z).mul(movementSpeed));
        System.out.println("Moving forward! [Position = X " + position.x + " Y " + position.y + " Z " + position.z);
    }

    public void moveBack() {
        position.add(new Vector3f(front.x, front.y, front.z).mul(-movementSpeed));
        System.out.println("Moving back! [Position = X " + position.x + " Y " + position.y + " Z " + position.z);
    }

    public void moveLeft() {
        position.add(new Vector3f(right.x, right.y, right.z).mul(-movementSpeed));
        System.out.println("Moving left! [Position = X " + position.x + " Y " + position.y + " Z " + position.z);
    }

    public void moveRight() {
        position.add(new Vector3f(right.x, right.y, right.z).mul(movementSpeed));
        System.out.println("Moving right! [Position = X " + position.x + " Y " + position.y + " Z " + position.z);
    }
}
