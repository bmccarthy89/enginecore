package com.game.engine.core.eventing;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.cleanup.Cleanable;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.eventing.api.EventHandlerStorage;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toUnmodifiableSet;

public class EventHandlerStorageImpl implements EventHandlerStorage, Cleanable {

    private static final Set<EventHandler> eventHandlers = new HashSet<>();
    private static final Object lock = new Object();
    private static EventHandlerStorageImpl instance = null;

    private EventHandlerStorageImpl() {
        CleanableStorage.getInstance().subscribe(this);
    }

    public static EventHandlerStorageImpl getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new EventHandlerStorageImpl();
            }
        }
        return instance;
    }

    @Override
    public void subscribe(EventHandler eventHandler) {
        eventHandlers.add(eventHandler);
    }

    @Override
    public void unsubscribe(EventHandler eventHandler) {
        eventHandlers.remove(eventHandler);
    }

    @Override
    public Set<EventHandler> getCurrentHandlers() {
        return eventHandlers.stream().collect(toUnmodifiableSet());
    }

    @Override
    public void clean() {
        eventHandlers.forEach(Cleanable::clean);
        eventHandlers.clear();
    }
}
