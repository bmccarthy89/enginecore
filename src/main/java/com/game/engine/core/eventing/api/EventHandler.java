package com.game.engine.core.eventing.api;

import com.game.engine.core.cleanup.Cleanable;

/**
 * Responsible for handling specific game events, events will be processed from a backlog managed by the {@link EventQueue}.
 */
public interface EventHandler extends Cleanable {
    /**
     * Returns true if the passed event can be handled by this event handler.
     * Example implementation :
     * <code>
     * return event instanceof MyCustomEvent;
     * </code>
     * @param event the event to be processed
     * @return true if the event can be used by this handler otherwise false.
     */
    boolean isForEvent(Event event);

    /**
     * This is where the event is processed, only events that return true to {@link #isForEvent(Event)}
     * will be passed to the handle method where they can be acted upon.
     * @param event
     */
    void handle(Event event);
}
