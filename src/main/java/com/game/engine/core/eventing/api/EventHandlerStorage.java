package com.game.engine.core.eventing.api;

import com.game.engine.core.cleanup.Cleanable;
import com.game.engine.core.eventing.EventHandlerStorageImpl;

import java.util.Set;

/**
 * There should only be one instance of the store to ensure that EventHandlers are in one place.
 * {@link EventHandlerStorageImpl} for this reason is a singleton store.
 * When the game is exited, all handler's that are registered at the time will have their respective
 * {@link Cleanable#clean()} method invoked and will be unregistered from the store.
 */
public interface EventHandlerStorage {
    /**
     * Adds the EventHandler to a set of handlers which will be used to process pre-rendering game events.
     * @param eventHandler the handler to be added to the store.
     */
    void subscribe(EventHandler eventHandler);

    /**
     * Remove the eventhandler from the store, meaning it will no longer be used to process in game events.
     * @param eventHandler the handler to be removed from the store.
     */
    void unsubscribe(EventHandler eventHandler);

    /**
     * Provides a copy of the registered handlers at the time of calling.
     * @return the current EventHanlder's that are registered.
     */
    Set<EventHandler> getCurrentHandlers();
}
