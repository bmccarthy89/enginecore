package com.game.engine.core.eventing.api;

import com.game.engine.core.cleanup.Cleanable;

/**
 * The EventQueue is used to add events to a queue which will be processed pre-rendering. There is no guarantee on
 * what from the event will be processed within.
 */
public interface EventQueue extends Cleanable {
    /**
     * Adds the event to the queue of events, returns true if successfully added.
     * An event can not be added if the queue is at it's capacity.
     * It is recommended to attempt to add events to the queue in a separate {@link Thread} to the {@link com.game.engine.core.GameLoop},
     * this will allow a certain number of retry attempts without blocking the processing of other events or rendering.
     * @param event the event to add to the queue.
     * @return true if the event was successfully added to the queue.
     */
    boolean add(Event event);

    /**
     * Removes the {@link Event} at the head of the queue and returns it to be processed.
     * @return the event that was at the head of the queue.
     */
    Event poll();
}
