package com.game.engine.core.eventing.api;

import com.game.engine.core.cleanup.Cleanable;

/**
 * Implementors of Feed will also implement the Runnable and Cleanable interface.
 * All feeds should be registered for initialisation where they have a thread allocated to them for the duration of their life.
 * Their responsibility is to produce instances of Event outside of the GameLoop.
 */
public interface Feed extends Runnable, Cleanable {
}
