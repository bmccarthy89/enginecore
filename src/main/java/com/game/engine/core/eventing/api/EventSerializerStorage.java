package com.game.engine.core.eventing.api;

import com.game.engine.core.cleanup.Cleanable;

import java.util.Set;

/**
 * Stores all {@link EventSerializer}'s
 */
public interface EventSerializerStorage extends Cleanable {
    /**
     * Adds the supplied serializer to the store
     * @param serializer the serializer to be added.
     */
    void subscribe(EventSerializer serializer);

    /**
     * Provides a copy of the registered serializers.
     * @return all event serializers.
     */
    Set<EventSerializer> getSerializers();
}
