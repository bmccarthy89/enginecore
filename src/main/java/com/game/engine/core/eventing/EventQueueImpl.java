package com.game.engine.core.eventing;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class EventQueueImpl implements EventQueue {

    private final ArrayBlockingQueue<Event> events = new ArrayBlockingQueue<>(1000, true);
    private final ConcurrentLinkedQueue<Event> backlog = new ConcurrentLinkedQueue<>();
    private static final Object lock = new Object();
    private static EventQueueImpl instance = null;

    private EventQueueImpl() {
        CleanableStorage.getInstance().subscribe(this);
    }

    public static EventQueue getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new EventQueueImpl();
            }
        }
        return instance;
    }

    @Override
    public boolean add(Event event) {
        if(events.offer(event)) {
            return true;
        } else {
            backlog.offer(event);
        }
        return events.offer(event);
    }

    @Override
    public Event poll() {
        Event poll = events.poll();
        reduceBacklog();
        return poll;
    }

    @Override
    public void clean() {
        events.clear();
    }

    private void reduceBacklog() {
        while (backlog.peek() != null && events.remainingCapacity() > 0) {
            events.offer(backlog.poll());
        }
    }
}
