package com.game.engine.core.eventing;

import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.eventing.api.EventSerializer;
import com.game.engine.core.eventing.api.EventSerializerStorage;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toUnmodifiableSet;

public class EventSerializerStorageImpl implements EventSerializerStorage {

    private final Set<EventSerializer> serializers = new HashSet<>();
    private static final Object lock = new Object();
    private static EventSerializerStorageImpl instance = null;

    private EventSerializerStorageImpl() {
        CleanableStorage.getInstance().subscribe(this);
    }
    public static EventSerializerStorage getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new EventSerializerStorageImpl();
            }
        }
        return instance;
    }

    @Override
    public void subscribe(EventSerializer serializer) {
        serializers.add(serializer);
    }

    @Override
    public Set<EventSerializer> getSerializers() {
        return serializers.stream().collect(toUnmodifiableSet());
    }

    @Override
    public void clean() {
        serializers.clear();
    }
}
