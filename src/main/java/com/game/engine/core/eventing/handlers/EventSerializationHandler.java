package com.game.engine.core.eventing.handlers;

import com.game.engine.core.eventing.EventSerializerStorageImpl;
import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.eventing.api.EventSerializer;
import com.game.engine.core.eventing.api.EventSerializerStorage;

public class EventSerializationHandler implements EventHandler {

    private final EventSerializerStorage serializerStore = EventSerializerStorageImpl.getInstance();

    @Override
    public boolean isForEvent(Event event) {
        //All events must be serializable
        return true;
    }

    @Override
    public void handle(Event event) {
        boolean eventHasHandler = false;
        for (EventSerializer serializer : serializerStore.getSerializers()) {
            if(serializer.isForEvent(event)) {
                eventHasHandler = true;
                serializer.write(event);
            }
        }
        if (!eventHasHandler) {
            throw new IllegalArgumentException("Event " + event.getClass().getSimpleName() + " has no registered serializer");
        }
    }

    @Override
    public void clean() {
        //do nothing
    }
}
