package com.game.engine.core.eventing.api;

/**
 * Implementations of this interface are expected to serialize the passed Event.
 * Serializers should be able to both read and write a single passed Event.
 * Events are Serialized into a file in the order in which they are processed within GameLoop and should only be
 * serialized once per unique event.
 * The serialized file enables us to replay events in the order in which they were received.
 */
public interface EventSerializer {
    /**
     * Writes the provided Event to the file, containing the current underlying state for the passed event. This should include
     * The full class path for the supplied Event so that it can be reconstructed in the same way.
     * @param event
     */
    void write(Event event);

    /**
     * Reads the Event from a serialized file of events, the Event should be reconstructed using the concrete class it was written as.
     * @return the Event that was read from File.
     */
    Event read();

    /**
     * Returns true if the passed event can be serialized by this event serializer.
     * Example implementation :
     * <code>
     * return event instanceof MyCustomEvent;
     * </code>
     * @param event the event to be processed
     * @return true if the event can be used by this handler otherwise false.
     */
    boolean isForEvent(Event event);
}
