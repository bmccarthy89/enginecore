package com.game.engine.core.eventing.api;

/**
 * Used as a generic type for event handlers, any implementation of {@link EventHandler} can be added to the {@link EventHandlerStorage} .
 * Events will be processed inside the {@link com.game.engine.core.GameLoop} and sent to all handlers.
 *
 * Any implementation of this class should only hold the relevant data for the given {@link Event}, and should not contain
 * any behaviour to aside from exposing the underlying data attached to the Event.
 */
public interface Event {}
