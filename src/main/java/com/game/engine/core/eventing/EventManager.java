package com.game.engine.core.eventing;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;
import com.game.engine.core.eventing.api.EventHandlerStorage;
import com.game.engine.core.eventing.api.EventQueue;

import java.util.Set;

/**
 * The EventManager class is used to process the {@link com.game.engine.core.eventing.api.EventQueue}
 * and send the polled {@link com.game.engine.core.eventing.api.Event}'s to all registered
 * {@link com.game.engine.core.eventing.api.EventHandler}'s in the {@link EventHandlerStorage}.
 */
public class EventManager {

    private final EventQueue eventQueue = EventQueueImpl.getInstance();
    private final EventHandlerStorage handlerStore = EventHandlerStorageImpl.getInstance();

    public void processEvents() {
        Event event = null;
        Set<EventHandler> currentHandlers = handlerStore.getCurrentHandlers();
        while ((event = eventQueue.poll()) != null) {
            for (EventHandler handler : currentHandlers) {
                if (handler.isForEvent(event)) {
                    handler.handle(event);
                }
            }
        }
    }

    public void processEventsRestrictedTime(long allowance) {
        Event event = null;
        Set<EventHandler> currentHandlers = handlerStore.getCurrentHandlers();
        long start = System.currentTimeMillis();
        long timeTaken = System.currentTimeMillis();
        while (timeTaken - start < allowance && (event = eventQueue.poll()) != null) {
            for (EventHandler handler : currentHandlers) {
                if (handler.isForEvent(event)) {
                    handler.handle(event);
                }
            }
        }
    }
}
