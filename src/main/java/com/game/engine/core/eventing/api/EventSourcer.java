package com.game.engine.core.eventing.api;

/**
 * Event sourcer is responsible for sourcing {@link Event} data and adding the concrete Event to the EventQueue.
 * Events should be sourced externally from the {@link com.game.engine.core.GameLoop}.
 */
public interface EventSourcer {

    /**
     * Used to source an event and add it to the {@link EventQueue}.
     */
    void source();
}
