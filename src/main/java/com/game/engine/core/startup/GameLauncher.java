package com.game.engine.core.startup;

import com.game.engine.core.GameLoop;
import com.game.engine.core.cleanup.CleanableStorage;
import com.game.engine.core.eventing.EventHandlerStorageImpl;
import com.game.engine.core.eventing.EventQueueImpl;
import com.game.engine.core.eventing.EventSerializerStorageImpl;
import com.game.engine.core.eventing.api.EventSerializerStorage;
import com.game.engine.core.eventing.handlers.EventSerializationHandler;
import com.game.engine.core.io.KeyHeldEventFeed;
import com.game.engine.core.io.KeyboardInputs;
import com.game.engine.core.io.MouseInputs;
import com.game.engine.core.io.Window;
import com.game.engine.core.io.eventing.events.MousePositionUpdatedEvent;
import com.game.engine.core.io.eventing.handlers.ButtonClickedEventHandler;
import com.game.engine.core.io.eventing.handlers.ButtonReleasedEventHandler;
import com.game.engine.core.io.eventing.handlers.KeyHeldEventHandler;
import com.game.engine.core.io.eventing.handlers.KeyPressedEventHandler;
import com.game.engine.core.io.eventing.handlers.KeyReleasedEventHandler;
import com.game.engine.core.io.eventing.handlers.MousePositionUpdatedEventHandler;
import com.game.engine.core.io.eventing.serializers.ButtonClickedEventSerializer;
import com.game.engine.core.io.eventing.serializers.ButtonReleasedEventSerializer;
import com.game.engine.core.io.eventing.serializers.KeyHeldEventSerializer;
import com.game.engine.core.io.eventing.serializers.KeyPressedEventSerializer;
import com.game.engine.core.io.eventing.serializers.KeyReleasedEventSerializer;
import com.game.engine.core.io.eventing.serializers.MousePositionMovedEventSerializer;

public class GameLauncher {

    public static void main(String[] args) {
        CleanableStorage cleanableStorage = CleanableStorage.getInstance();
        registerSerializers();
        registerEventHandlers();
        initialiseFeeds();

        Window window = new Window();
        window.createMainWindow();

        cleanableStorage.subscribe(window);
        KeyboardInputs keyboardInputs = new KeyboardInputs(window.getWindowId());
        keyboardInputs.createCallbacks();
        MouseInputs mouseInputs = new MouseInputs(window.getWindowId());
        mouseInputs.createCallbacks();

        EventQueueImpl.getInstance().add(new GameLaunchedEvent());
        EventQueueImpl.getInstance().add(new MousePositionUpdatedEvent(0.0f, 0.0f));
        try {
            GameLoop gameLoop = new GameLoop();
            gameLoop.doLoop(window.getWindowId());
        } catch (Throwable t) {
            cleanableStorage.doClean();
            t.printStackTrace();
            throw t;
        }
        cleanableStorage.doClean();
    }

    private static void registerEventHandlers() {
        EventHandlerStorageImpl eventHandlerStore = EventHandlerStorageImpl.getInstance();

        //Serialization handlers
        eventHandlerStore.subscribe(new EventSerializationHandler());

        //IO Event Handlers
        eventHandlerStore.subscribe(new ButtonClickedEventHandler());
        eventHandlerStore.subscribe(new ButtonReleasedEventHandler());
        eventHandlerStore.subscribe(new KeyPressedEventHandler());
        eventHandlerStore.subscribe(new KeyReleasedEventHandler());
        eventHandlerStore.subscribe(new MousePositionUpdatedEventHandler());
        eventHandlerStore.subscribe(new GameLaunchedEventHandler());
        eventHandlerStore.subscribe(new KeyHeldEventHandler());
    }

    private static void registerSerializers() {
        EventSerializerStorage serializerStore = EventSerializerStorageImpl.getInstance();

        // IO event serializers
        serializerStore.subscribe(new ButtonReleasedEventSerializer());
        serializerStore.subscribe(new ButtonClickedEventSerializer());
        serializerStore.subscribe(new KeyPressedEventSerializer());
        serializerStore.subscribe(new KeyReleasedEventSerializer());
        serializerStore.subscribe(new MousePositionMovedEventSerializer());
        serializerStore.subscribe(new GameLaunchedEventSerializer());
        serializerStore.subscribe(new KeyHeldEventSerializer());
    }

    private static void initialiseFeeds() {
        startRunnable(new KeyHeldEventFeed());
    }

    private static void startRunnable(Runnable runnable) {
        Thread thread = new Thread(runnable, runnable.getClass().getCanonicalName());
        thread.start();
    }
}
