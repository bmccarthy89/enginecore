package com.game.engine.core.startup;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventHandler;

public class GameLaunchedEventHandler implements EventHandler {

    @Override
    public void clean() {

    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof GameLaunchedEvent;
    }

    @Override
    public void handle(Event event) {

    }
}
