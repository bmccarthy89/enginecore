package com.game.engine.core.startup;

import com.game.engine.core.eventing.api.Event;
import com.game.engine.core.eventing.api.EventSerializer;

public class GameLaunchedEventSerializer implements EventSerializer {

    @Override
    public void write(Event event) {

    }

    @Override
    public Event read() {
        return null;
    }

    @Override
    public boolean isForEvent(Event event) {
        return event instanceof GameLaunchedEvent;
    }
}
