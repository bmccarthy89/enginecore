#version 330

in vec4 vertColour;
out vec4 colour;

void main()
{
    colour = vertColour;
}