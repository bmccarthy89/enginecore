package com.game.engine.core.eventing;

import com.game.engine.core.GameLoop;
import com.game.engine.core.camera.Camera;
import com.game.engine.core.eventing.api.EventQueue;
import com.game.engine.core.eventing.api.EventSerializerStorage;
import com.game.engine.core.io.Window;
import com.game.engine.core.io.eventing.events.KeyPressedEvent;
import com.game.engine.core.io.eventing.handlers.KeyPressedEventHandler;
import com.game.engine.core.io.eventing.serializers.KeyPressedEventSerializer;
import org.joml.Vector3f;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.lwjgl.glfw.GLFW;
import org.mockito.InOrder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EventQueueImplTest {

    private final EventQueue eventQueue = EventQueueImpl.getInstance();
    private final Window window = new Window();
    private final GameLoop loop = new GameLoop();

    @BeforeAll
    public static void oneTimeSetUp() {
        registerEventHandlers();
        registerSerializers();
    }

    @BeforeEach
    public void setUp() {
        eventQueue.clean();
        window.clean();
    }


    @Test
    public void all_events_processed_even_when_exceeding_size_limit_of_1000_events() {
        window.createMainWindow();

        List<KeyPressedEvent> events = new ArrayList<>();

        for (int i = 0; i <= 1001; i++) {
            KeyPressedEvent event = mockKeyPressedEvent(window.getWindowId(), i);
            eventQueue.add(event);
            events.add(event);
        }
        eventQueue.add(new KeyPressedEvent(window.getWindowId(), GLFW.GLFW_KEY_ESCAPE, GLFW.GLFW_PRESS));

        loop.doLoop(window.getWindowId());

        for (int i = 0; i <= 1001; i++) {
            KeyPressedEvent keyPressedEvent = events.get(i);
            verify(keyPressedEvent).getKey();
            verify(keyPressedEvent).getWindowId();
            verify(keyPressedEvent).getAction();
        }
        window.clean();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Test
    public void all_events_processed_in_the_order_they_are_added_to_the_queue() {
        window.createMainWindow();

        List<KeyPressedEvent> events = new ArrayList<>();

        for (int i = 0; i <= 1001; i++) {
            KeyPressedEvent event = mockKeyPressedEvent(window.getWindowId(), i);
            eventQueue.add(event);
            events.add(event);
        }

        eventQueue.add(new KeyPressedEvent(window.getWindowId(), GLFW.GLFW_KEY_ESCAPE, GLFW.GLFW_PRESS));
        loop.doLoop(window.getWindowId());

        InOrder order = inOrder(events.toArray());
        for (int i = 0; i <= 1001; i++) {
            KeyPressedEvent keyPressedEvent = events.get(i);
            order.verify(keyPressedEvent).getKey();
            order.verify(keyPressedEvent).getAction();
            order.verify(keyPressedEvent).getWindowId();
        }
        window.clean();
    }

    private KeyPressedEvent mockKeyPressedEvent(long windowId, int key) {
        KeyPressedEvent event = mock(KeyPressedEvent.class, "Event with key " + key);
        when(event.getWindowId()).thenReturn(windowId);
        when(event.getKey()).thenReturn(key);
        when(event.getAction()).thenReturn(GLFW.GLFW_PRESS);
        return event;
    }

    private static void registerEventHandlers() {
        EventHandlerStorageImpl eventHandlerStore = EventHandlerStorageImpl.getInstance();

        //IO Event Handlers
        eventHandlerStore.subscribe(new KeyPressedEventHandler());
    }

    private static void registerSerializers() {
        EventSerializerStorage serializerStore = EventSerializerStorageImpl.getInstance();

        // IO event serializers
        serializerStore.subscribe(new KeyPressedEventSerializer());
    }
}